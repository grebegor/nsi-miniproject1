from flask import render_template, redirect, url_for, request
from app import app
import datetime
import plotly.graph_objs as go
import plotly.io as pio

# Simulated temperature values
temperature_data = [
    {"timestamp": "2024-05-25 10:00:00", "temp": 20.5},
    {"timestamp": "2024-05-25 09:00:00", "temp": 19.8},
    {"timestamp": "2024-05-25 08:00:00", "temp": 21.1},
    {"timestamp": "2024-05-25 07:00:00", "temp": 20.3},
    {"timestamp": "2024-05-25 06:00:00", "temp": 19.5},
]


@app.route("/")
def home():
    return redirect(url_for("login"))


@app.route("/login")
def login():
    return render_template("login.html")


@app.route("/register")
def register():
    return render_template("register.html")


@app.route("/dashboard", methods=["GET", "POST"])
def dashboard():
    X = int(request.form.get("X", 5))
    Y = int(request.form.get("Y", 0))

    if Y > 0:
        del temperature_data[:Y]

    displayed_data = temperature_data[:X]

    # Prepare Plotly graph
    timestamps = [entry["timestamp"] for entry in displayed_data]
    temperatures = [entry["temp"] for entry in displayed_data]

    graph = {
        "data": [
            go.Scatter(
                x=timestamps, y=temperatures, mode="lines+markers", name="Temperature"
            )
        ],
        "layout": go.Layout(
            title="Temperature in time",
            xaxis={"title": "Timestamp"},
            yaxis={"title": "Temperature (°C)"},
        ),
    }
    graph_json = pio.to_json(graph)

    return render_template(
        "dashboard.html",
        temperature_data=displayed_data,
        total_entries=len(temperature_data),
        graph_json=graph_json,
    )


@app.route("/add_data", methods=["POST"])
def add_data():
    temp = float(request.form.get("temp"))
    timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    temperature_data.append({"timestamp": timestamp, "temp": temp})
    return redirect(url_for("dashboard"))

