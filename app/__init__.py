from flask import Flask
from flask_bootstrap import Bootstrap

app = Flask(__name__)
app.config["SECRET_KEY"] = "qwerty123"
Bootstrap(app)

from app import routes
